# Machine Learning Utils

This module contains applications dedicated to machine learning

## License

Please see the license for legal issues on the use of the software (GNU Affero General Public License v3.0).

## Contact

Rémi Cresson (IRSTEA)

