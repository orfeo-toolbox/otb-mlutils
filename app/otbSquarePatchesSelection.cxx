/*=========================================================================

  Copyright (c) Remi Cresson (IRSTEA). All rights reserved.


     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#include "otbWrapperApplicationFactory.h"

#include "otbSquarePatchesStatisticsFilter.h"

namespace otb
{


namespace Wrapper
{

class SquarePatchesSelection : public Application
{
public:
  /** Standard class typedefs. */
  typedef SquarePatchesSelection        Self;
  typedef Application                   Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Standard macro */
  itkNewMacro(Self);

  itkTypeMacro(SquarePatchesSelection, Application);

  typedef otb::SquarePatchesStatisticsFilter<FloatVectorImageType, UInt16VectorImageType> FilterType;

private:
  void DoInit() override
  {
    SetName("SquarePatchesSelection");
    SetDescription("Compute stats on patches");

    // Documentation
    SetDocLongDescription("This application computes some stats over patches.");
    SetDocLimitations("None");
    SetDocAuthors("Remi Cresson");
    SetDocSeeAlso(" ");

    AddParameter(ParameterType_InputImage, "in", "input");
    AddParameter(ParameterType_OutputImage, "out", "Output image");

    AddParameter(ParameterType_Int, "patchsize", "Patch size");
    SetDefaultParameterInt         ("patchsize", 64);
    SetMinimumParameterIntValue    ("patchsize", 2);

    AddParameter(ParameterType_Float, "nodata", "Nodata value");
    SetDefaultParameterFloat("nodata", 0.);

    AddRAMParameter();

  }

  void DoUpdateParameters() override
  {

  }


  void DoExecute() override
  {

    // Get the input
    FloatVectorImageType* img = GetParameterImage("in");

    // Filter
    m_Filter = FilterType::New();
    m_Filter->SetInput(img);
    m_Filter->SetNoDataValue(GetParameterFloat("nodata"));
    m_Filter->SetPatchSize(GetParameterInt("patchsize"));

    // Set the output image
    SetParameterOutputImage("out", m_Filter->GetOutput());

  }

  FilterType::Pointer m_Filter;
};

} // end namespace Wrapper
} // end namespace otb

OTB_APPLICATION_EXPORT(otb::Wrapper::SquarePatchesSelection)


