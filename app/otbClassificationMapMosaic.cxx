/*=========================================================================

  Copyright (c) Remi Cresson (IRSTEA). All rights reserved.


     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#include "itkFixedArray.h"
#include "itkObjectFactory.h"
#include "otbWrapperApplicationFactory.h"

// Application engine
#include "otbStandardFilterWatcher.h"
#include "itkFixedArray.h"

// Filter
#include "otbClassificationMosaicArgmaxFilter.h"

namespace otb
{

namespace Wrapper
{

class ClassificationMapMosaic : public Application
{
public:
  /** Standard class typedefs. */
  typedef ClassificationMapMosaic                   Self;
  typedef Application                         Superclass;
  typedef itk::SmartPointer<Self>             Pointer;
  typedef itk::SmartPointer<const Self>       ConstPointer;

  /** Standard macro */
  itkNewMacro(Self);
  itkTypeMacro(ClassificationMapMosaic, Application);

  /** Filters */
  typedef otb::ClassificationMosaicArgmaxFilter<FloatVectorImageType, FloatVectorImageType, double> ArgmaxFilterType;

  /** Interpolators typedefs */
  typedef itk::LinearInterpolateImageFunction<FloatVectorImageType, double>            LinearInterpolationType;
  typedef itk::NearestNeighborInterpolateImageFunction<FloatVectorImageType, double>   NearestNeighborInterpolationType;
  typedef otb::BCOInterpolateImageFunction<FloatVectorImageType>                       BCOInterpolationType;

  void DoUpdateParameters()
  {
  }

  void DoInit()
  {

    // Documentation
    SetName("ClassificationMapMosaic");
    SetDescription("Mosaic multiple classification maps");
    SetDocLongDescription("The application perform the mosaicing of multiple classification maps ");

    SetDocAuthors("Remi Cresson");

    AddDocTag(Tags::Learning);

    // Input
    AddParameter(ParameterType_InputImageList, "il", "Input classification maps");

    // Mode
    AddParameter(ParameterType_Choice, "mode", "Fusion mode");
    AddChoice("mode.argmax", "Argmax of the averages values");

    // Interpolators
    AddParameter(ParameterType_Choice, "interpolator", "Interpolation");
    SetParameterDescription           ("interpolator", "This group of parameters allows to define how the input image will be interpolated during resampling.");
    MandatoryOff                      ("interpolator");

    AddChoice              ("interpolator.linear", "Linear interpolation");
    SetParameterDescription("interpolator.linear", "Linear interpolation leads to average image quality but is quite fast");

    AddChoice              ("interpolator.nn", "Nearest Neighbor interpolation");
    SetParameterDescription("interpolator.nn", "Nearest neighbor interpolation leads to poor image quality, but it is fast.");

    AddChoice              ("interpolator.bco", "Bicubic interpolation");
    SetParameterDescription("interpolator.bco", "Bicubic interpolation leads to great image quality, but it is slow.");
    AddParameter(ParameterType_Radius, "interpolator.bco.radius", "Radius for bicubic interpolation");
    SetParameterDescription           ("interpolator.bco.radius", "Size of the bicubic interpolation filter. If the target pixel size "
        "is higher than the input pixel size, increasing this parameter will reduce aliasing artefacts.");
    SetDefaultParameterInt            ("interpolator.bco.radius", 2);

    // No-data
    AddParameter(ParameterType_Float, "nodata", "No-data value");
    SetDefaultParameterFloat         ("nodata", 0.0);

    // Output
    AddParameter(ParameterType_OutputImage, "out", "output classification map");
    SetDefaultOutputPixelType("out", ImagePixelType_uint8);

  }

  template <class TMosaicFilter>
  void SelectInterpolator(typename TMosaicFilter::Pointer filter)
  {
    if ( GetParameterString("interpolator").compare("linear") == 0 )
    {
      LinearInterpolationType::Pointer interpolator = LinearInterpolationType::New();
      filter->SetInterpolator(interpolator);
    }
    else if ( GetParameterString("interpolator").compare("nn") == 0 )
    {
      NearestNeighborInterpolationType::Pointer interpolator = NearestNeighborInterpolationType::New();
      filter->SetInterpolator(interpolator);
    }
    else if ( GetParameterString("interpolator").compare("bco") == 0 )
    {
      BCOInterpolationType::Pointer interpolator = BCOInterpolationType::New();
      interpolator->SetRadius(GetParameterInt("interpolator.bco.radius") );
      filter->SetInterpolator(interpolator);
    }
    else
    {
      otbAppLogFATAL("Unknown interpolator: " << GetParameterString("interpolator"))
    }
  }

  void DoExecute()
  {

    // Get the input image list
    FloatVectorImageListType::Pointer inputArray = this->GetParameterImageList("il");

    if (GetParameterAsString("mode") == "argmax")
    {
      otbAppLogINFO("Fusion mode is argmax");
      m_ArgmaxMosaicFilter = ArgmaxFilterType::New();
      for (unsigned int i = 0 ; i < inputArray->Size() ; i++)
      {
        m_ArgmaxMosaicFilter->PushBackInput(inputArray->GetNthElement(i) );
      }

      // Set nodata
      FloatVectorImageType::PixelType innodatapix, outnodatapix;
      innodatapix.SetSize(inputArray->GetNthElement(0)->GetNumberOfComponentsPerPixel());
      outnodatapix.SetSize(1);
      innodatapix.Fill(GetParameterFloat("nodata"));
      outnodatapix.Fill(GetParameterFloat("nodata"));
      m_ArgmaxMosaicFilter->SetNoDataInputPixel(innodatapix);
      m_ArgmaxMosaicFilter->SetNoDataOutputPixel(outnodatapix);

      SelectInterpolator<ArgmaxFilterType>(m_ArgmaxMosaicFilter);
    }

    SetParameterOutputImage("out", m_ArgmaxMosaicFilter->GetOutput());

  }

private:
  ArgmaxFilterType::Pointer m_ArgmaxMosaicFilter;
}; // end of class

} // end namespace wrapper
} // end namespace otb

OTB_APPLICATION_EXPORT( otb::Wrapper::ClassificationMapMosaic )
