/*=========================================================================

  Copyright (c) Remi Cresson (IRSTEA). All rights reserved.


     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#include "otbWrapperApplicationFactory.h"

#include "otbStreamingResampleImageFilter.h"

#include "otbBCOInterpolateImageFunction.h"
#include "itkNearestNeighborInterpolateImageFunction.h"

#include "otbRegionComparator.h"
#include "otbMultiChannelExtractROI.h"

namespace otb
{

enum
{
  Interpolator_BCO,
  Interpolator_NNeighbor,
  Interpolator_Linear
};

namespace Wrapper
{

class SuperSuperimpose : public Application
{
public:
  /** Standard class typedefs. */
  typedef SuperSuperimpose         Self;
  typedef Application                   Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Standard macro */
  itkNewMacro(Self);

  itkTypeMacro(SuperSuperimpose, Application);

  typedef itk::LinearInterpolateImageFunction<FloatVectorImageType, double>  LinInterpolatorType;
  typedef itk::NearestNeighborInterpolateImageFunction<FloatVectorImageType, double> NNInterpolatorType;
  typedef otb::BCOInterpolateImageFunction<FloatVectorImageType> BCOInterpolatorType;
  typedef otb::MultiChannelExtractROI<FloatVectorImageType::InternalPixelType,FloatVectorImageType::InternalPixelType> ExtractROIFilterType;
  typedef otb::StreamingResampleImageFilter<FloatVectorImageType, FloatVectorImageType> BasicResamplerType;

private:
  void DoInit() override
  {
    SetName("SuperSuperimpose");
    SetDescription("Using available image metadata, project one image onto another one");

    // Documentation
    SetDocLongDescription("This application performs the projection of an image into the geometry of another one.");
    SetDocLimitations("None");
    SetDocAuthors("Remi Cresson");
    SetDocSeeAlso(" ");

    AddDocTag(Tags::Geometry);
    AddDocTag("Superimposition");

    AddParameter(ParameterType_InputImage,   "inr",   "Reference input");
    SetParameterDescription("inr","The input reference image.");

    AddParameter(ParameterType_InputImage,   "inm",   "The image to reproject");
    SetParameterDescription("inm","The image to reproject into the geometry of the reference input.");

    AddParameter(ParameterType_OutputImage, "out", "Output image");
    SetParameterDescription("out", "Output reprojected image.");

    AddParameter(ParameterType_Float, "ratio", "Ratio between input image pixel spacing and output image pixel spacing");
    SetDefaultParameterFloat         ("ratio", 1.0);

    AddParameter(ParameterType_Int, "padding", "Padding radius (in pixels of the reprojected image)");
    SetDefaultParameterInt         ("padding", 0);

    AddParameter(ParameterType_Float, "fv", "Fill Value");
    SetParameterDescription("fv","Fill value for area outside the reprojected image");
    SetDefaultParameterFloat("fv", 0.);
    MandatoryOff("fv");

    // Interpolators
    AddParameter(ParameterType_Choice,   "interpolator", "Interpolation");
    SetParameterDescription("interpolator","This group of parameters allows defining how the input image will be interpolated during resampling.");

    AddChoice("interpolator.bco",    "Bicubic interpolation");
    SetParameterDescription("interpolator.bco", "Bicubic interpolation leads to very good image quality but is slow.");

    AddParameter(ParameterType_Radius, "interpolator.bco.radius", "Radius for bicubic interpolation");
    SetParameterDescription("interpolator.bco.radius","This parameter allows controlling the size of the bicubic interpolation filter. If the target pixel size is higher than the input pixel size, increasing this parameter will reduce aliasing artifacts.");
    SetDefaultParameterInt("interpolator.bco.radius", 2);

    AddChoice("interpolator.nn",     "Nearest Neighbor interpolation");
    SetParameterDescription("interpolator.nn","Nearest neighbor interpolation leads to poor image quality, but it is very fast.");

    AddChoice("interpolator.linear", "Linear interpolation");
    SetParameterDescription("interpolator.linear","Linear interpolation leads to average image quality but is quite fast");

    AddRAMParameter();

    // Doc example parameter settings
    SetDocExampleParameterValue("inr", "QB_Toulouse_Ortho_PAN.tif");
    SetDocExampleParameterValue("inm", "QB_Toulouse_Ortho_XS.tif");
    SetDocExampleParameterValue("out", "SuperSuperimposedXS_to_PAN.tif");

    SetOfficialDocLink();
  }

  void DoUpdateParameters() override
  {

  }


  void DoExecute() override
  {
    // Get the inputs
    FloatVectorImageType* refImage = GetParameterImage("inr");
    FloatVectorImageType* movingImage = GetParameterImage("inm");

    // Resample filter
    m_BasicResampler = BasicResamplerType::New();

    // Get Interpolator
    switch ( GetParameterInt("interpolator") )
    {
    case Interpolator_Linear:
    {
      LinInterpolatorType::Pointer interpolator = LinInterpolatorType::New();
      m_BasicResampler->SetInterpolator(interpolator);
    }
    break;
    case Interpolator_NNeighbor:
    {
      NNInterpolatorType::Pointer interpolator = NNInterpolatorType::New();
      m_BasicResampler->SetInterpolator(interpolator);
    }
    break;
    case Interpolator_BCO:
    {
      BCOInterpolatorType::Pointer interpolator = BCOInterpolatorType::New();
      interpolator->SetRadius(GetParameterInt("interpolator.bco.radius"));
      m_BasicResampler->SetInterpolator(interpolator);
    }
    break;
    }

    // Set up output image information
    FloatVectorImageType::SpacingType spacing = refImage->GetSignedSpacing();
    FloatVectorImageType::IndexType   start   = refImage->GetLargestPossibleRegion().GetIndex();
    FloatVectorImageType::SizeType    size    = refImage->GetLargestPossibleRegion().GetSize();
    FloatVectorImageType::PointType   origin  = refImage->GetOrigin();

    for (int i = 0 ; i < 2 ; i++)
    {
      spacing[i] *= GetParameterFloat("ratio");
      size[i] /= GetParameterFloat("ratio");
      size[i] += GetParameterInt("padding") * 2;
      origin[i] += 0.5*(spacing[i] - refImage->GetSignedSpacing()[i]) - GetParameterInt("padding") * spacing[i];
    }

    FloatVectorImageType::PixelType defaultValue;
    itk::NumericTraits<FloatVectorImageType::PixelType>::SetLength(defaultValue, movingImage->GetNumberOfComponentsPerPixel());
    defaultValue.Fill(GetParameterFloat("fv"));

    m_BasicResampler->SetInput(movingImage);
    m_BasicResampler->SetOutputOrigin(origin);
    m_BasicResampler->SetOutputSpacing(spacing);
    m_BasicResampler->SetOutputSize(size);
    m_BasicResampler->SetOutputStartIndex(start);
    m_BasicResampler->SetEdgePaddingValue(defaultValue);
    m_BasicResampler->UpdateOutputInformation();

    // Keep only overlap
    otb::RegionComparator<FloatVectorImageType, FloatVectorImageType> comparator;
    comparator.SetImage1(m_BasicResampler->GetOutput());
    comparator.SetImage2(movingImage);

    // Initialize ROI extract filters
    m_ExtractROI = ExtractROIFilterType::New();
    m_ExtractROI->SetInput(m_BasicResampler->GetOutput());
    m_ExtractROI->SetExtractionRegion(comparator.GetOverlapInImage1Indices());

    // Set the output image
    SetParameterOutputImage("out", m_ExtractROI->GetOutput());

  }

  BasicResamplerType::Pointer      m_BasicResampler;
  ExtractROIFilterType::Pointer    m_ExtractROI;
};

} // end namespace Wrapper
} // end namespace otb

OTB_APPLICATION_EXPORT(otb::Wrapper::SuperSuperimpose)


