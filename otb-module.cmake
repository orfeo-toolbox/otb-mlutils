set(DOCUMENTATION "Machine learning utils")

otb_module(MLUtils
  DEPENDS
    OTBITK
    OTBCommon
    OTBApplicationEngine
    OTBMosaic
    SimpleExtractionTools
    OTBProjection
    	
  TEST_DEPENDS
    OTBTestKernel
    OTBCommandLine
  DESCRIPTION
    $DOCUMENTATION
)
