/*=========================================================================

  Copyright (c) Remi Cresson (IRSTEA). All rights reserved.


     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#ifndef __ClassificationMosaicArgmaxFilter_H
#define __ClassificationMosaicArgmaxFilter_H

#include "otbStreamingMosaicFilterBase.h"

namespace otb
{
/** \class ClassificationMosaicArgmaxFilter
 * \brief Computes the mosaic of an input classification map set.
 * The filter computes the mosaic of the input classification map.
 * For each pixel, the returned value is the argmax.
 *
 * \ingroup MLUtils
 */
template <class TInputImage, class TOutputImage=TInputImage, class TInternalValueType=double>
class ITK_EXPORT ClassificationMosaicArgmaxFilter : public otb::StreamingMosaicFilterBase<TInputImage, TOutputImage,
                                                                                     TInternalValueType>
{
public:

  /** Standard Self typedef */
  typedef ClassificationMosaicArgmaxFilter                                                   Self;
  typedef otb::StreamingMosaicFilterBase<TInputImage, TOutputImage, TInternalValueType> Superclass;
  typedef itk::SmartPointer<Self>                                                       Pointer;
  typedef itk::SmartPointer<const Self>                                                 ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Runtime information support. */
  itkTypeMacro(ClassificationMosaicArgmaxFilter, StreamingMosaicFilterBase);

  /** Input image typedefs.  */
  typedef typename Superclass::InputImageType          InputImageType;
  typedef typename Superclass::InputImagePixelType     InputImagePixelType;
  typedef typename Superclass::IteratorType            IteratorType;
  typedef typename Superclass::InterpolatorPointerType InterpolatorPointerType;
  typedef typename Superclass::InputImageRegionType    InputImageRegionType;

  /** Output image typedefs.  */
  typedef typename Superclass::OutputImageType              OutputImageType;
  typedef typename Superclass::OutputImagePointType         OutputImagePointType;
  typedef typename Superclass::OutputImagePixelType         OutputImagePixelType;
  typedef typename Superclass::OutputImageInternalPixelType OutputImageInternalPixelType;
  typedef typename Superclass::OutputImageRegionType        OutputImageRegionType;

  /** Internal computing typedef support. */
  typedef typename Superclass::InternalValueType InternalValueType;
  typedef typename Superclass::InternalPixelType InternalPixelType;

protected:
  ClassificationMosaicArgmaxFilter() {
  }

  virtual ~ClassificationMosaicArgmaxFilter() {
  }

  /** Overrided methods */
  virtual void GenerateOutputInformation(void);
  virtual void ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, itk::ThreadIdType threadId );

private:
  ClassificationMosaicArgmaxFilter(const Self&); //purposely not implemented
  void operator=(const Self&);              //purposely not implemented

}; // end of class

} // end namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbClassificationMosaicArgmaxFilter.hxx"
#endif

#endif
