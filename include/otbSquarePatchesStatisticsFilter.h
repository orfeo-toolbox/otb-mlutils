/*=========================================================================

  Copyright (c) Remi Cresson (IRSTEA). All rights reserved.


     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#ifndef __SquarePatchesStatisticsFilter_H
#define __SquarePatchesStatisticsFilter_H

#include "itkImageToImageFilter.h"
#include <vector>
#include "vnl/vnl_matrix.h"

namespace otb
{
/** \class SquarePatchesStatisticsFilter
 * \brief Computes some stats in the patches.
 * Patches are sampled as a matrix of adjacent patches.
 *
 * \ingroup MLUtils
 */
template <class TInputImage, class TOutputImage>
class ITK_EXPORT SquarePatchesStatisticsFilter : public itk::ImageToImageFilter<TInputImage, TOutputImage>
{
public:

  /** Standard Self typedef */
  typedef SquarePatchesStatisticsFilter                      Self;
  typedef itk::ImageToImageFilter<TInputImage, TOutputImage> Superclass;
  typedef itk::SmartPointer<Self>                            Pointer;
  typedef itk::SmartPointer<const Self>                      ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Runtime information support. */
  itkTypeMacro(SquarePatchesStatisticsFilter, itk::ImageToImageFilter);

  /** Input image typedefs.  */
  typedef TInputImage                            InputImageType;
  typedef typename InputImageType::PixelType     InputImagePixelType;
  typedef typename InputImageType::InternalPixelType InputImageValueType;
  typedef typename itk::ImageRegionConstIterator<TInputImage> IteratorType;
  typedef typename InputImageType::RegionType    InputImageRegionType;
  typedef typename InputImageType::IndexType     InputImageIndexType;
  typedef typename InputImageType::SizeType      InputImageSizeType;
  typedef typename InputImageType::PointType     InputImagePointType;
  typedef typename InputImageType::Pointer       InputImagePointer;
  typedef typename InputImageType::SpacingType   InputImageSpacingType;

  /** Output image typedefs.  */
  typedef TOutputImage                                OutputImageType;
  typedef typename OutputImageType::PointType         OutputImagePointType;
  typedef typename OutputImageType::PixelType         OutputImagePixelType;
  typedef typename OutputImageType::InternalPixelType OutputImageInternalPixelType;
  typedef typename OutputImageType::RegionType        OutputImageRegionType;
  typedef typename itk::ImageRegionIterator<TOutputImage>   OutIteratorType;

  typedef vnl_matrix<OutputImageInternalPixelType> CountMatrixType;

  itkSetMacro(PatchSize, unsigned int);
  itkSetMacro(NoDataValue, InputImageValueType);

protected:
  SquarePatchesStatisticsFilter() {
  m_NoDataValue = 0;
  m_PatchSize = 0;
  }

  virtual ~SquarePatchesStatisticsFilter() {
  }

  /** Overrided methods */
  virtual void GenerateOutputInformation();
  virtual void GenerateInputRequestedRegion();
  virtual void BeforeThreadedGenerateData();
  virtual void ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, itk::ThreadIdType threadId );
  virtual void AfterThreadedGenerateData();

private:
  SquarePatchesStatisticsFilter(const Self&); //purposely not implemented
  void operator=(const Self&);              //purposely not implemented

  unsigned int                 m_PatchSize;
  InputImageValueType          m_NoDataValue;
  std::vector<CountMatrixType> m_ThreadsMasks;

}; // end of class

} // end namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbSquarePatchesStatisticsFilter.hxx"
#endif

#endif
