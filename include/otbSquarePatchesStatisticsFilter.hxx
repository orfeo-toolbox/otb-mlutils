/*=========================================================================

  Copyright (c) Remi Cresson (IRSTEA). All rights reserved.


     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#ifndef __SquarePatchesStatisticsFilter_txx
#define __SquarePatchesStatisticsFilter_txx

#include "otbSquarePatchesStatisticsFilter.h"

namespace otb {


template <class TInputImage, class TOutputImage>
void
SquarePatchesStatisticsFilter<TInputImage, TOutputImage>
::GenerateOutputInformation()
 {

  Superclass::GenerateOutputInformation();

  //////////////////////////////////////////////////////////////////////////////////////////
  //                            Compute the output image extent
  //////////////////////////////////////////////////////////////////////////////////////////

  // Spacing
  const InputImageSpacingType m_InputSpacing = this->GetInput()->GetSignedSpacing();
  InputImageSpacingType m_OutputSpacing = m_InputSpacing;
  for (unsigned int dim = 0 ; dim < 2 ; dim++)
    m_OutputSpacing[dim] *= m_PatchSize;

  // Origin
  InputImagePointType m_OutputOrigin = this->GetInput()->GetOrigin();
  for (unsigned int dim = 0 ; dim < 2 ; dim++)
    {
    m_OutputOrigin[dim] -= 0.5 * m_InputSpacing[dim];
    m_OutputOrigin[dim] += 0.5 * m_OutputSpacing[dim];
    }

  // Size
  InputImageSizeType m_OutputSize = this->GetInput()->GetLargestPossibleRegion().GetSize();
  for (unsigned int dim = 0 ; dim < 2 ; dim++)
    m_OutputSize[dim] /= m_PatchSize;
  InputImageRegionType largestPossibleRegion;
  largestPossibleRegion.SetSize(m_OutputSize);

  // Number of components per pixel
  unsigned int outputPixelSize = 1;

  // Copy input image projection
  InputImageType * inputImage = static_cast<InputImageType * >( Superclass::ProcessObject::GetInput(0) );
  const std::string projectionRef = inputImage->GetProjectionRef();

  // Set output image origin/spacing/size/projection
  OutputImageType * outputPtr = this->GetOutput();
  outputPtr->SetNumberOfComponentsPerPixel(outputPixelSize);
  outputPtr->SetProjectionRef        ( projectionRef );
  outputPtr->SetOrigin               ( m_OutputOrigin );
  outputPtr->SetSignedSpacing        ( m_OutputSpacing );
  outputPtr->SetLargestPossibleRegion( largestPossibleRegion );

 }

template <class TInputImage, class TOutputImage>
void
SquarePatchesStatisticsFilter<TInputImage, TOutputImage>
::GenerateInputRequestedRegion()
 {
  Superclass::GenerateInputRequestedRegion();

  // Output requested region
  OutputImageRegionType requestedRegion = this->GetOutput()->GetRequestedRegion();

  // Input requested region
  for (unsigned int dim = 0 ; dim < 2 ; dim++)
    {
    requestedRegion.GetModifiableIndex()[dim] *= m_PatchSize;
    requestedRegion.GetModifiableSize()[dim] *= m_PatchSize;
    }
  InputImageType * inputImage = static_cast<InputImageType * >(Superclass::ProcessObject::GetInput(0) );
  inputImage->SetRequestedRegion(requestedRegion);

 }

/*
 * Before threaded generation
 */
template <class TInputImage, class TOutputImage>
void
SquarePatchesStatisticsFilter<TInputImage, TOutputImage>
::BeforeThreadedGenerateData()
 {
  m_ThreadsMasks.clear();
  const InputImageRegionType reqRegion = this->GetInput()->GetRequestedRegion();
  const InputImageSizeType reqSize = reqRegion.GetSize();
  for (unsigned int i = 0; i < this->GetNumberOfThreads(); i++)
    m_ThreadsMasks.push_back(CountMatrixType(reqSize[0], reqSize[1], 0));

 }

/**
 * Processing
 */
template <class TInputImage, class TOutputImage>
void
SquarePatchesStatisticsFilter<TInputImage, TOutputImage>
::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, itk::ThreadIdType threadId)
{

  // Output region --> Input region
  InputImageIndexType newStart = outputRegionForThread.GetIndex();
  InputImageSizeType newSize = outputRegionForThread.GetSize();
  for (unsigned int dim = 0; dim < 2; dim++)
    {
    newStart[dim] *= m_PatchSize;
    newSize[dim] *= m_PatchSize;
    }
  InputImageRegionType inputRegion(newStart, newSize);

  // Iterate through the input image
  const InputImagePointer inputPtr = const_cast<TInputImage*>(this->GetInput());
  IteratorType it(inputPtr, inputRegion);
  for (it.GoToBegin(); !it.IsAtEnd(); ++it)
    {
    const InputImageValueType val = it.Get()[0];
    const InputImageIndexType idx = it.GetIndex();
    m_ThreadsMasks[threadId][idx[0]/m_PatchSize][idx[1]/m_PatchSize] += static_cast<OutputImageInternalPixelType>(val != m_NoDataValue);
    }

}

template <class TInputImage, class TOutputImage>
void
SquarePatchesStatisticsFilter<TInputImage, TOutputImage>
::AfterThreadedGenerateData()
 {
  const InputImagePointer inputPtr = const_cast<TInputImage*>(this->GetInput());
  const InputImageRegionType reqRegion = inputPtr->GetRequestedRegion();
  const InputImageSizeType reqSize = reqRegion.GetSize();
  CountMatrixType result(reqSize[0], reqSize[1], 0);

  // Reduce threads
  unsigned int i,j;
  for (auto& threadMask: m_ThreadsMasks)
    for (i=0; i<reqSize[0]; i++)
      for (j=0; j<reqSize[1]; j++)
        result[i][j] += threadMask[i][j];

  // Set output
  typename TOutputImage::Pointer outputPtr = this->GetOutput();
  const OutputImageRegionType outputReqRegion = outputPtr->GetRequestedRegion();
  OutIteratorType it(outputPtr, outputReqRegion);
  OutputImagePixelType outPix;
  outPix.SetSize(1);
  for (it.GoToBegin(); !it.IsAtEnd(); ++it)
    {
    const InputImageIndexType idx = it.GetIndex();
    outPix[0] = result[(unsigned int) idx[0]][(unsigned int) idx[1]];
    it.Set(outPix);
    }
 }


} // end namespace gtb

#endif
