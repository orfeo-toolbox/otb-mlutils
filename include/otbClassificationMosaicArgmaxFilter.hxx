/*=========================================================================

  Copyright (c) Remi Cresson (IRSTEA). All rights reserved.


     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#ifndef __ClassificationMosaicArgmaxFilter_txx
#define __ClassificationMosaicArgmaxFilter_txx

#include "otbClassificationMosaicArgmaxFilter.h"

namespace otb {

template <class TInputImage, class TOutputImage, class TInternalValueType>
void
ClassificationMosaicArgmaxFilter<TInputImage, TOutputImage, TInternalValueType>
::GenerateOutputInformation(void)
{
  Superclass::GenerateOutputInformation();
  
  // Output pixel has 1 channel
  unsigned int outNbBands = 1;
  OutputImageType * outputPtr = this->GetOutput();
  outputPtr->SetNumberOfComponentsPerPixel(outNbBands);

  // No data value is 255
  OutputImagePixelType noDataPix;
  noDataPix.SetSize(outNbBands);
  noDataPix.Fill(255);
  this->SetNoDataOutputPixel(noDataPix);
}

/**
 * Processing
 */
template <class TInputImage, class TOutputImage, class TInternalValueType>
void
ClassificationMosaicArgmaxFilter<TInputImage, TOutputImage, TInternalValueType>
::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, itk::ThreadIdType threadId)
{

  // Debug info
  itkDebugMacro(<<"Actually executing thread " << threadId << " in region " << outputRegionForThread);

  // Support progress methods/callbacks
  itk::ProgressReporter progress(this, threadId, outputRegionForThread.GetNumberOfPixels() );

  // Get output pointer
  OutputImageType * mosaicImage = this->GetOutput();

  // Get number of used inputs
  const unsigned int nbOfUsedInputImages = Superclass::GetNumberOfUsedInputImages();

  // Get number of bands
  const unsigned int nBands = Superclass::GetNumberOfBands();

  // Iterate through the thread region
  IteratorType outputIt(mosaicImage, outputRegionForThread);

  // Prepare interpolated pixel
  InternalPixelType interpolatedMathPixel;
  interpolatedMathPixel.SetSize(nBands);

  // Prepare input pointers, interpolators, and valid regions (input images)
  typename std::vector<InputImageType *>        currentImage;
  typename std::vector<InterpolatorPointerType> interp;
  Superclass::PrepareImageAccessors(currentImage, interp);

  // Container for geo coordinates
  OutputImagePointType geoPoint;
  InputImagePixelType tmpPixel(Superclass::GetNoDataInputPixel() );
  bool isData;
  for ( outputIt.GoToBegin(); !outputIt.IsAtEnd(); ++outputIt )
    {
    // Prepare output pixel
    tmpPixel.Fill(0);
    isData = false;
    OutputImagePixelType outPixel(Superclass::GetNoDataOutputPixel() );

    // Current pixel --> Geographical point
    mosaicImage->TransformIndexToPhysicalPoint (outputIt.GetIndex(), geoPoint) ;

    // Loop on used input images
    for (unsigned int i = 0 ; i < nbOfUsedInputImages ; i++)
      {
      // Check if the point is inside the transformed thread region
      // (i.e. the region in the current input image which match the thread
      // region)
      if (interp[i]->IsInsideBuffer(geoPoint) )
        {

        // Compute the interpolated pixel value
        InputImagePixelType interpolatedPixel = interp[i]->Evaluate(geoPoint);

        // Check that interpolated pixel is not empty
        if (Superclass::IsPixelNotEmpty(interpolatedPixel) )
          {
          // Update the output pixel
          isData = true;
          for (unsigned int band = 0 ; band < nBands ; band++)
            {
            tmpPixel[band] += interpolatedPixel[band];
            }
          } // Interpolated pixel is not empty
        }   // point inside buffer
      }     // next image

    // Update output pixel value
    if (isData)
      {
      OutputImageInternalPixelType argmax = 0;
      float max = -1.0;
      for (unsigned int band = 0 ; band < nBands ; band++)
        {
        if (tmpPixel[band] > max)
          {
          argmax = band;
          max = tmpPixel[band];
          }
        }
      outPixel[0] = argmax;
      }
    outputIt.Set(outPixel);

    // Update progress
    progress.CompletedPixel();

    } // next output pixel

}

} // end namespace gtb

#endif
